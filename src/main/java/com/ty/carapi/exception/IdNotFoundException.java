package com.ty.carapi.exception;

public class IdNotFoundException extends RuntimeException{
    private String message="Given Id not found";

    public IdNotFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}

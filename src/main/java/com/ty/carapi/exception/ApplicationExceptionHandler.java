package com.ty.carapi.exception;

import com.ty.carapi.dto.Car;
import com.ty.carapi.util.ResponseStructure;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(IdNotFoundException.class)
    public ResponseEntity<ResponseStructure<String>> handleIdNotFoundException(IdNotFoundException exception){
       ResponseStructure<String>responseStructure=new ResponseStructure<>();
        responseStructure.setStatus(HttpStatus.NOT_FOUND.value());
        responseStructure.setMessage("Id not Found");
        responseStructure.setData(exception.getMessage());
        return new ResponseEntity<>(responseStructure,HttpStatus.NOT_FOUND);
    }
}

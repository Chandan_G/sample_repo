package com.ty.carapi.controller;

import com.ty.carapi.dto.Car;
import com.ty.carapi.service.CarService;
import com.ty.carapi.util.ResponseStructure;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cars")
@Slf4j
public class CarController {

    Logger logger= LoggerFactory.getLogger(CarController.class);
    @Autowired
    CarService carService;

    @PostMapping()
    public ResponseEntity<ResponseStructure<Car>> saveCar(@RequestBody Car car){
        return carService.saveCar(car);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseStructure<Car>>  getCarById(@PathVariable int id){
        return carService.getCarById(id);
    }

    @GetMapping()
    public ResponseEntity<ResponseStructure<List<Car>>> getAll(){
        logger.info("info message");
        logger.warn("warn message");
        logger.error("error message");
        logger.debug("debug message");
        logger.trace("trace message");
        return carService.getAll();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseStructure<Car>>  deleteCar(@PathVariable int id){
        return carService.deleteCar(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseStructure<Car>> updateCar(@PathVariable int id,@RequestBody Car car){
        return carService.updateCar(id, car);
    }
}
package com.ty.carapi.repository;

import com.ty.carapi.dto.Car;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CarRepository extends MongoRepository<Car,Integer> {
}

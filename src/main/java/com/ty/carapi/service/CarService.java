package com.ty.carapi.service;

import com.ty.carapi.dao.CarDao;
import com.ty.carapi.dto.Car;
import com.ty.carapi.exception.IdNotFoundException;
import com.ty.carapi.util.ResponseStructure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarService {

    //Altered
    @Autowired
    CarDao carDao;

    public ResponseEntity<ResponseStructure<Car>> saveCar(Car car){
        ResponseStructure<Car> responseStructure=new ResponseStructure<>();
        responseStructure.setStatus(HttpStatus.CREATED.value());
        responseStructure.setMessage("CREATED");
        responseStructure.setData(carDao.saveCar(car));
        return new ResponseEntity<ResponseStructure<Car>>(responseStructure,HttpStatus.CREATED);
    }

    public ResponseEntity<ResponseStructure<Car>>  getCarById(int id){
        Optional<Car> opt =carDao.getCarById(id);
        if(opt.isPresent()){
            ResponseStructure<Car> responseStructure = new ResponseStructure<>();
            responseStructure.setStatus(HttpStatus.FOUND.value());
            responseStructure.setMessage("Id Found");
            responseStructure.setData(opt.get());
            return new ResponseEntity<ResponseStructure<Car>>(responseStructure, HttpStatus.FOUND);
        }
        else{
            throw new IdNotFoundException("Given ID " + id + " not found");

        }
    }

    public ResponseEntity<ResponseStructure<List<Car>>> getAll(){
        ResponseStructure<List<Car>> responseStructure=new ResponseStructure<>();
        responseStructure.setStatus(HttpStatus.OK.value());
        responseStructure.setMessage("SUCCESS");
        responseStructure.setData(carDao.getAll());
        return new ResponseEntity<ResponseStructure<List<Car>>>(responseStructure,HttpStatus.OK);
    }

    public ResponseEntity<ResponseStructure<Car>>  deleteCar(int id){
        Optional<Car> opt =carDao.getCarById(id);
        if(opt.isPresent()){
          carDao.deleteCar(opt.get());
            ResponseStructure<Car> responseStructure=new ResponseStructure<>();
            responseStructure.setStatus(HttpStatus.OK.value());
            responseStructure.setMessage("Deleted");
            responseStructure.setData(opt.get());
            return new ResponseEntity<ResponseStructure<Car>>(responseStructure,HttpStatus.OK);
        }
        else{
            throw new IdNotFoundException("Given ID " + id + " not found");

        }
    }

    public ResponseEntity<ResponseStructure<Car>> updateCar(int id,Car car){
        Optional<Car> opt =carDao.updateCar(id);
        if(opt.isPresent()){
           Car c =opt.get();
           c.setName(car.getName());
           c.setBrand(car.getBrand());
            ResponseStructure<Car> responseStructure=new ResponseStructure<>();
            responseStructure.setStatus(HttpStatus.OK.value());
            responseStructure.setMessage("Updated");
            responseStructure.setData(carDao.saveCar(c));
            return new ResponseEntity<ResponseStructure<Car>>(responseStructure,HttpStatus.OK);
        }
        else{
            throw new IdNotFoundException("Given ID " + id + " not found");
        }
    }
}

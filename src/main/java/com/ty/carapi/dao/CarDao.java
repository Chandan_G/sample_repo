package com.ty.carapi.dao;

import com.ty.carapi.dto.Car;
import com.ty.carapi.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CarDao {
    @Autowired
    CarRepository carRepository;

    public Car saveCar(Car car){
        return carRepository.save(car);
    }

    public Optional<Car> getCarById(int id){
        return carRepository.findById(id);
    }

    public List<Car> getAll(){
        return carRepository.findAll();
    }

    public void deleteCar(Car car){
        carRepository.delete(car);
    }

    public Optional<Car> updateCar(int id){
        return carRepository.findById(id);
    }

}
